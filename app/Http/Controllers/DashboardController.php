<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        return view('dashboard',[
            'user' => $user
        ]);
    }
}
