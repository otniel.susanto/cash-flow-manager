<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $transaksi = Models\Transaksi::orderBy('id')->paginate(8);
        $kategori = Models\Kategori::all();
        return view('transaksi.index',[
            'transaksi' => $transaksi,
            'kategori' => $kategori,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $jenis_transaksi = Models\JenisTransaksi::get();
        // dd($jenis_transaksi);
        return view('transaksi.create',[
            'user' => $user,
            'jenis_transaksi' => $jenis_transaksi,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nominal' => 'required',
            'tanggal' => 'required',
            'kategori_id' => 'required',
            'jenis_transaksi_id' => 'required'
        ]);
        $nominal = $request->nominal;
        if($request->jenis_transaksi_id == 2){
            $nominal = $nominal * -1;
        }
        $transaksi = new Models\Transaksi();
        $transaksi->nominal = $request->nominal;
        $transaksi->tanggal = $request->tanggal;
        $transaksi->kategori_id = $request->kategori_id;
        $transaksi->jenis_transaksi_id = $request->jenis_transaksi_id;
        $transaksi->konfirmasi = 0;
        $transaksi->keterangan = $request->keterangan;
        $transaksi->save();
        $user = Auth::user();
        $user->saldo = $user->saldo + $nominal;
        $user->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data transaksi telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $transaksi = Models\Transaksi::find($id);
        return response()->json([ 
            'error' => false, 
            'transaksi' => $transaksi,
            'user' => $user
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $transaksi = Models\Transaksi::find($id);
        return response()->json([ 
            'error' => false, 
            'transaksi' => $transaksi,
            'user' => $user
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nominal' => 'required',
            'tanggal' => 'required',
            'kategori_id' => 'required',
            'jenis_transaksi_id' => 'required'
        ]);
        $transaksi = Models\Transaksi::find($id);
        $transaksi->nominal = $request->nominal;
        $transaksi->tanggal = $request->tanggal;
        $transaksi->kategori_id = $request->kategori_id;
        $transaksi->jenis_transaksi_id = $request->jenis_transaksi_id;
        $transaksi->konfirmasi = 0;
        $transaksi->keterangan = $request->keterangan;
        $transaksi->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data transaksi telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Models\Transaksi::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data transaksi telah dihapus", 
        ], 200);
    }
}
