<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $kategori = Models\Kategori::orderBy('id')->paginate(8);
        return view('kategori.index',[
            'kategori' => $kategori,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('kategori.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nama' => 'required'
        ]);
        $kategori = new Models\Kategori();
        $kategori->nama = $request->nama;
        $kategori->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $kategori = Models\Kategori::find($id);
        return response()->json([ 
            'error' => false, 
            'kategori' => $kategori, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Models\Kategori::find($id);
        return response()->json([ 
            'error' => false, 
            'kategori' => $kategori, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        $kategori = Models\Kategori::find($id);
        $kategori->nama = $request->nama;
        $kategori->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Models\Kategori::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori telah dihapus", 
        ], 200);
    }

    public function getByJenis(Request $request){
        $kategori = Models\Kategori::where($request->jenis_transaksi_id);
        return response()->json([ 
            'kategori' => $kategori
        ], 200);
    }
}
