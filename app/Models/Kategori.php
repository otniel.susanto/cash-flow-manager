<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    public function transaksi(){
        return $this->hasMany('App\Models\Transaksi', 'kategori_id','id');
    }
}
