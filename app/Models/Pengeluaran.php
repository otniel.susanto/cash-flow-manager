<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = 'pengeluaran';
    public function kategori(){
        return $this->belongsTo('App\Models\Kategori', 'kategori_id');
    }
}
