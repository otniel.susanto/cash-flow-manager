<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    public function kategori(){
        return $this->belongsTo('App\Models\Kategori', 'kategori_id');
    }
    public function jenisTransaksi(){
        return $this->belongsTo('App\Models\JenisTransaksi', 'jenis_transaksi_id');
    }
}
