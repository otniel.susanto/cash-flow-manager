<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisTransaksi extends Model
{
    protected $table = 'jenis_transaksi';
    public function transaksi(){
        return $this->hasMany('App\Models\Transaksi', 'jenis_transaksi_id','id');
    }
}
