<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditTransaksi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditTransaksi">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Transaksi </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> 
                        × 
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-transaksi-errors"> </ul>
                    </div>
                    <div class="form-group"> 
                        <label> Nominal </label> 
                        <input class="form-control" id="edit-nominal" required type="text">
                    </div>
                    <div class="form-group"> 
                        <label>Tanggal</label> 
                        <input class="form-control" id="edit-tanggal" required type="date">
                    </div>
                    <div class="form-group"> 
                        <label for="edit-kategori_id">Kategori</label>
                        <select id="edit-kategori_id" class="select2 form-control">
                            @foreach($kategori as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="jenis_transaksi_id">Jenis Transaksi</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="edit-jenis_transaksi_id" checked value="1">
                        <label class="form-check-label" for="Pemasukan">
                            Pemasukan
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="edit-jenis_transaksi_id" value="2">
                        <label class="form-check-label" for="Pengeluaran">
                        Pengeluaran
                        </label>
                    </div>
                    <div class="form-group"> 
                        <label> Keterangan </label> 
                        <input class="form-control" id="edit-keterangan" required="" type="text">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <input id="id_edit" type="hidden" value="0"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-info" id="btnUpdate" type="button" value="edit"> 
                        <i class="fa fa-pen"></i> 
                        Update 
                    </button> 
                </div>
            </form>
        </div>
    </div>
</div>
