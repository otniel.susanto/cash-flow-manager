@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Transaksi</h1>

        <a onclick="event.preventDefault();formAddTransaksi();"
            class="add btn btn-primary float-right m-1 open-modal" data-toggle="modal">
            <i class="fa fa-edit"></i>
            Tambah Data
        </a>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('transaksi')}}">Transaksi</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nominal</th>
                    <th width="">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transaksi as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditTransaksi({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteTransaksi({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$transaksi->count()}}</b> out of <b>{{$transaksi->total()}}</b> entries
            </div> {{ $transaksi->links() }}
        </div>
    </div>
    @include('transaksi.create')
    @include('transaksi.edit')
    @include('transaksi.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("transaksi")}}',
                data: {
                    nominal: $("#add-nominal").val(),
                    tanggal: $("#add-tanggal").val(),
                    kategori_id: $("#add-kategori_id").val(),
                    jenis_transaksi_id: $('input[name=add-jenis_transaksi_id]:checked').val(),
                    keterangan: $("#add-keterangan").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('transaksi')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    var message = ""
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        message += value;
                    });
                    $("#add-error-bag").show();
                    alert(message);
                }
            });
        });
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("transaksi")}}/' + $("#formEditTransaksi #id_edit").val(),
                data: {
                    nominal: $("#edit-nominal").val(),
                    tanggal: $("#edit-tanggal").val(),
                    kategori_id: $("#edit-kategori_id").val(),
                    jenis_transaksi_id: $('input[name=edit-jenis_transaksi_id]:checked').val(),
                    keterangan: $("#edit-keterangan").val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditTransaksi').trigger("reset");
                    $("#formEditTransaksi .close").click();
                    alert(data.message);
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-transaksi-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-transaksi-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("transaksi")}}/' + $("#formDeleteTransaksi #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteTransaksi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formAddTransaksi(id) {
        $("#add-error-bag").hide();
        $('#modalAddTransaksi').modal('show');
    }

    function formEditTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("transaksi")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#edit-nominal").val(data.transaksi.nominal);
                $("#edit-tanggal").val(data.transaksi.tanggal);
                $("#edit-kategori_id").val(data.transaksi.nominal);
                $("#edit-kategori_id").change();
                $("#editketerangan").val(data.transaksi.keterangan);
                $("#id_edit").val(data.transaksi.id);
                $('#modalEditTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("transaksi")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteTransaksi #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Transaksi nominal: " + data.transaksi.nominal + "</p>" + 
                "<p>Keterangan: " + data.transaksi.keterangan + "</p>")
                $("#formDeleteTransaksi #id_delete").val(data.transaksi.id);
                $('#modalDeleteTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
