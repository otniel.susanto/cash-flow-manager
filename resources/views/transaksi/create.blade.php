<!-- Add Modal HTML -->
<div class="modal fade" id="modalAddTransaksi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formAddTransaksi">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Transaksi </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> 
                        × 
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="add-error-bag">
                        <ul id="add-transaksi-errors"> </ul>
                    </div>
                    <div class="form-group"> 
                        <label> Nominal </label> 
                        <input class="form-control" id="add-nominal" required type="text">
                    </div>
                    <div class="form-group"> 
                        <label>Tanggal</label> 
                        <input class="form-control" id="add-tanggal" required type="date">
                    </div>
                    <div class="form-group"> 
                        <label for="add-kategori_id">Kategori</label>
                        <select id="add-kategori_id" class="select2 form-control">
                            @foreach($kategori as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label for="jenis_transaksi_id">Jenis Transaksi</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="add-jenis_transaksi_id" id="Pemasukan" checked value="1">
                        <label class="form-check-label" for="Pemasukan">
                            Pemasukan
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="add-jenis_transaksi_id" id="Pengeluaran" value="2">
                        <label class="form-check-label" for="Pengeluaran">
                        Pengeluaran
                        </label>
                    </div>
                    <div class="form-group"> 
                        <label> Keterangan </label> 
                        <input class="form-control" id="add-keterangan" required="" type="text">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-info" id="btnAdd" type="button" value="add"> 
                        <i class="fa fa-save"></i> 
                        Simpan 
                    </button> 
                </div>
            </form>
        </div>
    </div>
</div>