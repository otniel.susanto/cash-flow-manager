@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data JenisTransaksi</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('jenis_transaksi')}}">JenisTransaksi</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('jenis_transaksi/create')}}">Tambah</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddJenisTransaksi">
            <div class="form-group row">
                <div class="col-3">
                    <label for="nominal">Nominal</label>
                    <input required type="number" class="form-control" name="nominal" id="nominal" placeholder="Masukkan nominal">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-3">
                    <label for="jenis_jenis_transaksi">Jenis JenisTransaksi</label>
                    @foreach($jenis_jenis_transaksi as $key=>$itemJenisJenisTransaksi)
                    <div class="form-check">
                        <input class="form-check-input" type="radio" 
                        name="jenis_jenis_transaksi" 
                        id="jenis_jenis_transaksi{{$itemJenisJenisTransaksi->id}}"
                        @if($key==0) checked @endif value="{{$itemJenisJenisTransaksi->id}}">
                        <label class="form-check-label" for="jenis_jenis_transaksi{{$itemJenisJenisTransaksi->id}}">
                            {{$itemJenisJenisTransaksi->nama}}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="form-group row">
                <div class="col-3">
                    <label for="tanggal">Tanggal</label>
                    <input required type="date" class="form-control" name="tanggal" id="tanggal" placeholder="Tanggal/Bulan/Tahun">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <label for="keterangan">Keterangan</label>
                    <input required type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan keterangan JenisTransaksi">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-plus"></i> Tambah</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
$(document).ready(function () {
    $("#btnAdd").click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url:'{{url("jenis_transaksi")}}',
            data: {
                nominal: $("#nominal").val(),
                tanggal: $("#tanggal").val(),
                jenis_jenis_transaksi: $('input[name=jenis_jenis_transaksi]:checked').val(),
                keterangan: $("#keterangan").val()
            },
            dataType: 'json',
            success: function (data) {
                alert(data.message);
                window.location.replace("{{url('jenis_transaksi')}}");
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                var message = ""
                $('#add-errors').html('');
                $.each(errors.message, function (key, value) {
                    message += value;
                });
                $("#add-error-bag").show();
                alert(message);
            }
        });
    });
});
</script>
@endpush