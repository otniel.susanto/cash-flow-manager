@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>JenisTransaksi</h1>
        <a href="{{url('jenis_transaksi/create')}}" type="button" class="btn btn-primary float-right m-1">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('jenis_transaksi')}}">JenisTransaksi</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nominal</th>
                    <th width="">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jenis_transaksi as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditJenisTransaksi({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteJenisTransaksi({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$jenis_transaksi->count()}}</b> out of <b>{{$jenis_transaksi->total()}}</b> entries
            </div> {{ $jenis_transaksi->links() }}
        </div>
    </div>
    @include('jenis_transaksi.edit')
    @include('jenis_transaksi.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("jenis-transaksi")}}/' + $("#formEditJenisTransaksi #id_edit").val(),
                data: {
                    nominal: $("#formEditJenisTransaksi #nominal").val(),
                    keterangan: $("#formEditJenisTransaksi #keterangan").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditJenisTransaksi').trigger("reset");
                    $("#formEditJenisTransaksi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-jenis_transaksi-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-jenis_transaksi-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("jenis-transaksi")}}/' + $("#formDeleteJenisTransaksi #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteJenisTransaksi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditJenisTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-transaksi")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditJenisTransaksi #nama").val(data.jenis_transaksi.nama);
                $("#formEditJenisTransaksi #id_edit").val(data.jenis_transaksi.id);
                $('#modalEditJenisTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteJenisTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-transaksi")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteJenisTransaksi #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Jenis Transaksi: " + data.jenis_transaksi.nama + "</p>")
                $("#formDeleteJenisTransaksi #id_delete").val(data.jenis_transaksi.id);
                $('#modalDeleteJenisTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
