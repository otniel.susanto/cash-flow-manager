<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 text-sm">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
        <!-- <img src="" alt="" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
        <span class="brand-text font-weight-light">Cash Flow Manager</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info text-md">
            <a href="{{url('profile')}}" class="d-block">{{$user->name}}</a>
        </div>
      </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Dashboard (buat bapak) -->
                <li class="nav-item">
                    <a href="{{url('dashboard')}}" class="nav-link 
                        @if(str_contains(url()->current(), 'dashboard')))
                            active
                        @endif">
                        <i class="fa fa-tachometer-alt"></i> 
                        <p>Dashboard</p>
                    </a>
                </li>
                <!-- Mutasi (buat anak) -->
                <li class="nav-item">
                    <a href="{{url('transaksi')}}" class="nav-link 
                        @if(str_contains(url()->current(), 'transaksi')))
                            active
                        @endif">
                        <i class="fa fa-star"></i> 
                        <p>Mutasi</p>
                    </a>
                </li>
                <!-- Kategori -->
                <li class="nav-item">
                    <a href="{{url('kategori')}}" class="nav-link 
                        @if(str_contains(url()->current(), 'kategori')))
                            active
                        @endif">
                        <i class="fa fa-star"></i> 
                        <p>Kategori</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
