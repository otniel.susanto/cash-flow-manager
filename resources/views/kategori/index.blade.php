@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Kategori Transaksi</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('kategori')}}">Kategori Transaksi</a></li>
        </ol>
        <a href="{{url('kategori/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nama</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kategori as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditKategori({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKategori({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$kategori->count()}}</b> out of <b>{{$kategori->total()}}</b> entries
            </div> {{ $kategori->links() }}
        </div>
    </div>
    @include('kategori.edit')
    @include('kategori.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("kategori")}}/' + $("#formEditKategori #id_edit").val(),
                data: {
                    nama: $("#formEditKategori #nama").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditKategori').trigger("reset");
                    $("#formEditKategori .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-kategori-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-kategori-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("kategori")}}/' + $("#formDeleteKategori #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteKategori .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditKategori(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kategori")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditKategori #nama").val(data.kategori.nama);
                $("#formEditKategori #id_edit").val(data.kategori.id);
                $('#modalEditKategori').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteKategori(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kategori")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteKategori #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Jenis cpl: " + data.kategori.nama + "</p>")
                $("#formDeleteKategori #id_delete").val(data.kategori.id);
                $('#modalDeleteKategori').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
