@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Transaksi</h1>
        <a href="{{url('transaksi/create')}}" type="button" class="btn btn-primary float-right m-1">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('transaksi')}}">Transaksi</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

    </div>
    @include('transaksi.create')
    @include('transaksi.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("transaksi")}}/' + $("#formDeleteTransaksi #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteTransaksi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("transaksi")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditTransaksi #nominal").val(data.transaksi.nominal);
                $("#formEditTransaksi #keterangan").val(data.transaksi.keterangan);
                $("#formEditTransaksi #id_edit").val(data.transaksi.id);
                $('#modalEditTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteTransaksi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("transaksi")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteTransaksi #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Transaksi nominal: " + data.transaksi.nominal + "</p>" + 
                "<p>Keterangan: " + data.transaksi.keterangan + "</p>")
                $("#formDeleteTransaksi #id_delete").val(data.transaksi.id);
                $('#modalDeleteTransaksi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
