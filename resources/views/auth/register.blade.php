@extends('auth.layout')
@section('content')
<!-- .register-box -->
<div class="register-box">
    <div class="register-logo">
        <img src="{{asset('img/')}}" alt="Logo Cash Flow Manager" style="width:70px; height:80px;">
        <h5>Cash Flow Manager</h5>
    </div>
    <!-- /.register-logo -->
    <div class="card">
        <div class="card-body register-card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="input-group mb-3">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                        value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama Lengkap">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input id="username" type="text" class="form-control @error('name') is-invalid @enderror" name="username"
                        value="{{ old('username') }}" required autocomplete="name" autofocus placeholder="Username">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <!-- Email -->
                <div class="input-group mb-3">
                    <input id="email" type="text"  name="email" class="form-control @error('email') is-invalid @enderror"
                        value="{{ old('email') }}" required autocomplete="email" placeholder="email@email.com">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-at"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <!-- Saldo Awal -->
                <div class="input-group mb-3">
                    <input id="saldo" type="number" class="form-control @error('saldo') is-invalid @enderror" name="saldo"
                        value="{{ old('saldo') }}" required autocomplete="name" placeholder="Nominal Saldo Awal">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-money"></span>
                        </div>
                    </div>
                    @error('saldo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <!-- Password -->
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="new-password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>


                <div class="input-group mb-3">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                        required autocomplete="new-password" placeholder="Ketik Ulang Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" id="btnSubmit" class="btn btn-primary btn-block">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
            <img src="" class="d-none" style="display:none;" id="imgUser">
            <!-- /.social-auth-links -->
            <p class="mb-0">
                <a href="{{url('login')}}" class="text-center">Kembali ke halaman Login</a>
            </p>
        </div>
        <!-- /.register-card-body -->
    </div>
</div>
<!-- /.register-box -->
@endsection