<?php

use Illuminate\Database\Seeder;

class JenisTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_transaksi')->insert([
            'nama' => 'Pemasukan'
        ]);
        DB::table('jenis_transaksi')->insert([
            'nama' => 'Pengeluaran'
        ]);
    }
}
