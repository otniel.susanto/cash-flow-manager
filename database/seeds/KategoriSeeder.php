<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori')->insert([
            'nama' => 'Makanan'
        ]);
        DB::table('kategori')->insert([
            'nama' => 'Kebutuhan Sehari-hari'
        ]);
        DB::table('kategori')->insert([
            'nama' => 'Iuran'
        ]);
        DB::table('kategori')->insert([
            'nama' => 'Gaji'
        ]);
    }
}
