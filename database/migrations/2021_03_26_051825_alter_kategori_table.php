<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kategori', function (Blueprint $table) {
            $table->unsignedBigInteger('jenis_transaksi_id')->nullable();
            $table->foreign('jenis_transaksi_id')->references('id')->on('jenis_transaksi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kategori', function (Blueprint $table) {
            $table->dropForeign(['jenis_transaksi_id']);
            $table->dropColumn('jenis_transaksi_id');
        });
    }
}
